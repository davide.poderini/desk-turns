import json
import networkx as nx
import itertools as it
import os
import random
import argparse

class Room():
    def __init__(self, desk_names=[]):
        
        self._g = nx.Graph()
        self._g.add_nodes_from(desk_names)
            
    def _name_to_num(self, name):
        return list(self._g.nodes()).index(name) + 1
    
    def _num_to_name(self, num):
        return list(self._g.nodes())[num - 1]
    
    @property
    def desk_names(self):
        return list(self._g.nodes())
    
    def add_desks(self, desk_names):
        self._g.add_nodes_from(desk_names)
        
    def add_neighbours(self, a, b):
        if type(a) == int:
            a = self._num_to_name(a)
        if type(b) == int:
            b = self._num_to_name(b)
            
        self._g.add_edge(a,b)
        
    def remove_neighbours(self, a, b):
        if type(a) == int:
            a = self._num_to_name(a)
        if type(b) == int:
            b = self._num_to_name(b)
            
        self._g.remove_edge(a,b)
        
    def add_neighbours_clique(self, dset):
        for i, d in enumerate(dset):
            if type(d) == int:
                dset[i] = self._num_to_name(d)
        self._g.add_edges_from(it.combinations(dset, 2))
    
    def distant_desks(self, desks = None):
        if desks is None:
            notg = nx.complement(self._g)
        else:
            notg = nx.complement(nx.subgraph(self._g, desks)) 
        dd = list(nx.find_cliques(notg))
        dd.sort(key = lambda x: len(x), reverse = True)
        return dd
        
    def get_turns(self, desks = None, comb_no = None):
        turns = []
        if desks is None:
            desks = set(self._g.nodes)
        if not type(desks) is set:
            desks = set(desks)
        distd = self.distant_desks(desks = desks)
        
        found = False
        stop_at_min_no = False
        if comb_no is None or comb_no < 2:
            comb_no = len(distd)
            stop_at_min_no = True
            
        for n in range(comb_no):
            for comb in it.combinations(distd, n + 1):
                if set().union(*[set(d) for d in comb]) == desks:
                    turns.append(comb)
                    found = True
            if found and stop_at_min_no: break
        return turns
    
    def draw(self):
        nx.draw(self._g, labels = {x: self._name_to_num(x) for x in self._g.nodes},)
        
    def load(self, path):
        with open(path, "r") as f:
            room_dict = json.load(f)
        self.add_desks(room_dict['desks'])
        for a, b in room_dict['neighbours']:
            self.add_neighbours(a, b)
    
    def dump(self, path):
        room_dict = {
            'desks': list(self._g.nodes),
            'neighbours': list(self._g.edges),
        }
        with open(path, "w") as f:
            json.dump(room_dict, f)

def load_rooms(rdir = None):
    rooms = {}
    if rdir is None:
        rdid = os.curdir
    for rfile in filter(lambda x: x.endswith(".json"), os.listdir()):
        room = Room()
        room.load(rfile)
        rooms[rfile.rsplit(".")[0]] = room
    return rooms

def print_turn(room, all_names, title = None):
    names = set(all_names).intersection(set(room.desk_names))
    if len(names) == 0: 
        return
    turns = random.choice(room.get_turns(desks = names))
    if not title is None:
        print("="*(len(title) +1))
        print(title)
        print("="*(len(title) +1))
    print("Turn\tName\n-----\t----")
    for n in names:
        for i, t in enumerate(turns):
            if n in t: break
        print(i+1, "\t", n)
    print()
    
if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description="Simple script to obtain room shifts rotation.")
    parser.add_argument("names", type=str, metavar="name", nargs='+', default=None, help="Desk names (default: all).")
    parser.add_argument("-r", "--rooms-dir", type=str, metavar="dir", nargs=1, default=None, help="Directory were the files containing the rooms' specifications are stored (default: current directory).")
    
    args = parser.parse_args()
    rooms_dict = load_rooms(args.rooms_dir)
    for rn, room in rooms_dict.items():
        print_turn(room, args.names, title = rn)
            

    